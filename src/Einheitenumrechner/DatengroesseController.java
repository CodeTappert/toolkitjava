package Einheitenumrechner;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Klasse welche die FXML Datei DatengroesseRechner steuert
 * <p></p>
 *
 * @author Jannik Tappert
 * @version 1.0
 */
public class DatengroesseController implements Initializable {
    @FXML
    ComboBox<String> CBEingabe, CB1, CB2, CB3, CB4, CB5, CB6, CB7, CB8, CB9, CB10;
    @FXML
    TextField TFStart, TF1, TF2, TF3, TF4, TF5, TF6, TF7, TF8, TF9, TF10;
    @FXML
    Label L1, L2, L3, L4, L5, L6, L7, L8, L9, L10;
    @FXML
    Button BtnCalc, BtnBack;
    @FXML
    RadioButton RadioExp;
    String SCB1, SCB2, SCB3, SCB4, SCB5, SCB6, SCB7, SCB8, SCB9, SCB10, Nutzereingabe, SBit, SKilobit, SKibibit, SMegabit, SMebibit, SGigabit, SGibibit, STerabit, STebibit, SPetabit, SPebibit, SByte, SKilobyte,
            SKibibyte, SMegabyte, SMebibyte, SGigabyte, SGibibyte, STerabyte, STebibyte, SPetabyte, SPebibyte, LeereEingabe = "Es wurde nichts eingegeben";
    int RadioSelected = 1;
    Object Starteinheit, E1 = "Bit", E2 = "Kilobit", E3 = "Kibibit", E4 = "Megabit", E5 = "Mebibit", E6 = "Gigabit", E7 = "Gibibit", E8 = "Terabit", E9 = "Tebibit", E10 = "Petabit", E11 = "Pebibit",
            E12 = "Byte", E13 = "Kilobyte", E14 = "Kibibyte", E15 = "Megabyte", E16 = "Mebibyte", E17 = "Gigabyte", E18 = "Gibibyte", E19 = "Terabyte", E20 = "Tebibyte", E21 = "Petabyte", E22 = "Pebibyte";
    ObservableList<String> optionsDataSize = FXCollections.observableArrayList("Bit", "Kilobit", "Kibibit", "Megabit", "Mebibit", "Gigabit", "Gibibit", "Terabit", "Tebibit", "Petabit", "Pebibit", "Byte", "Kilobyte",
            "Kibibyte", "Megabyte", "Mebibyte", "Gigabyte", "Gibibyte", "Terabyte", "Tebibyte", "Petabyte", "Pebibyte");
    double EingabeInGigabyte, EBit, EKilobit, EKibibit, EMegabit, EMebibit, EGigabit, EGibibit, ETerabit, ETebibit, EPetabit, EPebibit, EByte, EKilobyte,
            EKibibyte, EMegabyte, EMebibyte, EGigabyte, EGibibyte, ETerabyte, ETebibyte, EPetabyte, EPebibyte;


    /**
     * Zurück zur Übersicht.
     */
    @FXML
    public void BtnBack() {
        Methoden.FXMLLoad("/Einheitenumrechner/EinheitenumrechnerUebersicht.fxml");
    }

    /**
     * Überprüft ob die RadioBox angewählt ist und setzt einen entsprechenden Integer Wert.
     * 0 = angewählt, 1 = nicht angewählt.
     */
    public void RadioExp() {
        if (RadioExp.isSelected()) {
            RadioSelected = 0;
        } else {
            RadioSelected = 1;
        }

    }

    /**
     * Knopfdruck auf Umrechnen. Holt sich zuerst die ausgewählte Einheit, dann die Nutzereingabe, überprüft diese ob diese eine ganzahlige Dezimalzahl ist.
     * Falls ja wird diese Eingabe in Gigabyte umgerechnet und dann von der Einheit in alle anderen Einheiten.
     * Falls der Radio Button nicht ausgeählt ist, wird die Eingabe so formatiert, dass keine Exponenten angezeigt werden.
     * Danach wird für jede der ErgebnissComboBoxen die Methode ErgebnisseComboBoxenZuordnen aufgerufen damit entsprechend die richtigen Einheiten zu den
     * ComboBoxen ausgewählt werden und die entsprechenden Ergebnisse denen zugeorndet werden.
     * Danach werden die Ergebnisse in die entsprechenden Textfelder gesetzt.
     * Falls nein wird Methode.KeineDez in die Textfelder gesetzt.
     * Falls nichts eingeben wurde wird der String LeereEingabe in die Textfelder gesetzt.
     *
     * @see Methoden
     */
    public void BtnCalc() {
        SetStarteinheit();
        SetNutzereingabe();
        int IsDez = Methoden.checkIfDez(Nutzereingabe);
        if (!Nutzereingabe.isEmpty()) {
            if (IsDez == 1) {
                InGigabyte();
                VonGigabyte();
                if (RadioSelected == 1) {
                    Formatierung();
                } else {
                    DoubleToString();
                }
                ComboBoxAufruf();
                ErgebnisseSetzen();
            } else {
                FehlerSetzen(Methoden.KeineDez);
            }
        } else {
            FehlerSetzen(LeereEingabe);
        }
    }

    /**
     * Setzt den Wert von Starteinheit auf die in der ComboBox ausgewählte Einheit.
     */
    public void SetStarteinheit() {
        Starteinheit = CBEingabe.getValue();
    }

    /**
     * Setzt den Wert von Nutzereingabe auf den in der Textbox TFStart eingegebenen String.
     */
    public void SetNutzereingabe() {
        Nutzereingabe = TFStart.getText();
    }

    /**
     * Wandelt je nach Eingabe des Nutzers und der von ihm ausgewählten Einheit in die Einheit Gigabyte und speichert den Wert in EingabeInGigabyte.
     */
    public void InGigabyte() {
        Object Einheit = Starteinheit;
        Double DEingabe = Double.parseDouble(Nutzereingabe);
        if (Einheit.equals(E1)) {
            EingabeInGigabyte = DEingabe * 1e-9;
        } else if (Einheit.equals(E2)) {
            EingabeInGigabyte = DEingabe * 1.25e-7;
        } else if (Einheit.equals(E3)) {
            EingabeInGigabyte = DEingabe * 1.28e-7;
        } else if (Einheit.equals(E4)) {
            EingabeInGigabyte = DEingabe * 0.000125;
        } else if (Einheit.equals(E5)) {
            EingabeInGigabyte = DEingabe * 0.000131072;
        } else if (Einheit.equals(E6)) {
            EingabeInGigabyte = DEingabe * 0.125;
        } else if (Einheit.equals(E7)) {
            EingabeInGigabyte = DEingabe * 0.134218;
        } else if (Einheit.equals(E8)) {
            EingabeInGigabyte = DEingabe * 125;
        } else if (Einheit.equals(E9)) {
            EingabeInGigabyte = DEingabe * 137.439;
        } else if (Einheit.equals(E10)) {
            EingabeInGigabyte = DEingabe * 125000;
        } else if (Einheit.equals(E11)) {
            EingabeInGigabyte = DEingabe * 140737;
        } else if (Einheit.equals(E12)) {
            EingabeInGigabyte = DEingabe * 1e-9;
        } else if (Einheit.equals(E13)) {
            EingabeInGigabyte = DEingabe * 1e-6;
        } else if (Einheit.equals(E14)) {
            EingabeInGigabyte = DEingabe * 1.024e-6;
        } else if (Einheit.equals(E15)) {
            EingabeInGigabyte = DEingabe * 0.001;
        } else if (Einheit.equals(E16)) {
            EingabeInGigabyte = DEingabe * 0.00104858;
        } else if (Einheit.equals(E17)) {
            EingabeInGigabyte = DEingabe;
        } else if (Einheit.equals(E18)) {
            EingabeInGigabyte = DEingabe * 1.07374;
        } else if (Einheit.equals(E19)) {
            EingabeInGigabyte = DEingabe * 1000;
        } else if (Einheit.equals(E20)) {
            EingabeInGigabyte = DEingabe * 1099.51;
        } else if (Einheit.equals(E21)) {
            EingabeInGigabyte = DEingabe * 1e+6;
        } else if (Einheit.equals(E22)) {
            EingabeInGigabyte = DEingabe * 1.126e+6;
        }
    }

    /**
     * Wandelt den Wert EingabeInGigabyte in alle Einheiten zurück und speichert dies in die Ergebnissvariablen.
     */
    public void VonGigabyte() {
        EBit = EingabeInGigabyte / 1e-9;
        EKilobit = EingabeInGigabyte / 1.25e-7;
        EKibibit = EingabeInGigabyte / 1.28e-7;
        EMegabit = EingabeInGigabyte / 0.000125;
        EMebibit = EingabeInGigabyte / 0.000131072;
        EGigabit = EingabeInGigabyte / 0.125;
        EGibibit = EingabeInGigabyte / 0.134218;
        ETerabit = EingabeInGigabyte / 125;
        ETebibit = EingabeInGigabyte / 137.439;
        EPetabit = EingabeInGigabyte / 125000;
        EPebibit = EingabeInGigabyte / 140737;
        EByte = EingabeInGigabyte / 1e-9;
        EKilobyte = EingabeInGigabyte / 1e-6;
        EKibibyte = EingabeInGigabyte / 1.024e-6;
        EMegabyte = EingabeInGigabyte / 0.001;
        EMebibyte = EingabeInGigabyte / 0.00104858;
        EGigabyte = EingabeInGigabyte;
        EGibibyte = EingabeInGigabyte / 1.07374;
        ETerabyte = EingabeInGigabyte / 1000;
        ETebibyte = EingabeInGigabyte / 1099.51;
        EPetabyte = EingabeInGigabyte / 1e+6;
        EPebibyte = EingabeInGigabyte / 1.126e+6;
    }

    /**
     * Formatierungsmethode welche Dezimalzahlen  ohne Exponent darstellen lässt.
     */
    public void Formatierung() {
        DecimalFormat format = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
        format.setMaximumFractionDigits(20);
        SBit = format.format(EBit);
        SKilobit = format.format(EKilobit);
        SKibibit = format.format(EKibibit);
        SMegabit = format.format(EMegabit);
        SMebibit = format.format(EMebibit);
        SGigabit = format.format(EGigabit);
        SGibibit = format.format(EGibibit);
        STerabit = format.format(ETerabit);
        STebibit = format.format(ETebibit);
        SPetabit = format.format(EPetabit);
        SPebibit = format.format(EPebibit);
        SByte = format.format(EByte);
        SKilobyte = format.format(EKilobyte);
        SKibibyte = format.format(EKibibyte);
        SMegabyte = format.format(EMegabyte);
        SMebibyte = format.format(EMebibyte);
        SGigabyte = format.format(EGigabyte);
        SGibibyte = format.format(EGibibyte);
        STerabyte = format.format(ETerabyte);
        STebibyte = format.format(ETebibyte);
        SPetabyte = format.format(EPetabyte);
        SPebibyte = format.format(EPebibyte);
    }

    /**
     * Wandelt die Ergebniss Double´s in Ergebniss Strings um.
     */
    public void DoubleToString() {
        SBit = Double.toString(EBit);
        SKilobit = Double.toString(EKilobit);
        SKibibit = Double.toString(EKibibit);
        SMegabit = Double.toString(EMegabit);
        SMebibit = Double.toString(EMebibit);
        SGigabit = Double.toString(EGigabit);
        SGibibit = Double.toString(EGibibit);
        STerabit = Double.toString(ETerabit);
        STebibit = Double.toString(ETebibit);
        SPetabit = Double.toString(EPetabit);
        SPebibit = Double.toString(EPebibit);
        SByte = Double.toString(EByte);
        SKilobyte = Double.toString(EKilobyte);
        SKibibyte = Double.toString(EKibibyte);
        SMegabyte = Double.toString(EMegabyte);
        SMebibyte = Double.toString(EMebibyte);
        SGigabyte = Double.toString(EGigabyte);
        SGibibyte = Double.toString(EGibibyte);
        STerabyte = Double.toString(ETerabyte);
        STebibyte = Double.toString(ETebibyte);
        SPetabyte = Double.toString(EPetabyte);
        SPebibyte = Double.toString(EPebibyte);
    }

    /**
     * Ruft für jede ComboBox die Methode ErgebnisseComboBoxenZuordnen auf.
     */
    public void ComboBoxAufruf() {
        ErgebnisseComboBoxenZuordnen(CB1, 1);
        ErgebnisseComboBoxenZuordnen(CB2, 2);
        ErgebnisseComboBoxenZuordnen(CB3, 3);
        ErgebnisseComboBoxenZuordnen(CB4, 4);
        ErgebnisseComboBoxenZuordnen(CB5, 5);
        ErgebnisseComboBoxenZuordnen(CB6, 6);
        ErgebnisseComboBoxenZuordnen(CB7, 7);
        ErgebnisseComboBoxenZuordnen(CB8, 8);
        ErgebnisseComboBoxenZuordnen(CB9, 9);
        ErgebnisseComboBoxenZuordnen(CB10, 10);

    }

    /**
     * Methode welche der ComboBox mit dem Index das richtige Ergebniss/Einheit zuordnet.
     *
     * @param CB      ComboBox welche überprüft werden soll.
     * @param IndexCB Index der Combobox
     */
    public void ErgebnisseComboBoxenZuordnen(ComboBox<String> CB, int IndexCB) {
        String Ergebniss = "";
        Object Einheit = CB.getValue();
        if (Einheit.equals(E1)) {
            Ergebniss = SBit;
        } else if (Einheit.equals(E2)) {
            Ergebniss = SKilobit;
        } else if (Einheit.equals(E3)) {
            Ergebniss = SKibibit;
        } else if (Einheit.equals(E4)) {
            Ergebniss = SMegabit;
        } else if (Einheit.equals(E5)) {
            Ergebniss = SMebibit;
        } else if (Einheit.equals(E6)) {
            Ergebniss = SGigabit;
        } else if (Einheit.equals(E7)) {
            Ergebniss = SGibibit;
        } else if (Einheit.equals(E8)) {
            Ergebniss = STerabit;
        } else if (Einheit.equals(E9)) {
            Ergebniss = STebibit;
        } else if (Einheit.equals(E10)) {
            Ergebniss = SPetabit;
        } else if (Einheit.equals(E11)) {
            Ergebniss = SPebibit;
        } else if (Einheit.equals(E12)) {
            Ergebniss = SByte;
        } else if (Einheit.equals(E13)) {
            Ergebniss = SKilobyte;
        } else if (Einheit.equals(E14)) {
            Ergebniss = SKibibit;
        } else if (Einheit.equals(E15)) {
            Ergebniss = SMegabyte;
        } else if (Einheit.equals(E16)) {
            Ergebniss = SMebibit;
        } else if (Einheit.equals(E17)) {
            Ergebniss = SGigabyte;
        } else if (Einheit.equals(E18)) {
            Ergebniss = SGibibyte;
        } else if (Einheit.equals(E19)) {
            Ergebniss = STerabyte;
        } else if (Einheit.equals(E20)) {
            Ergebniss = STebibyte;
        } else if (Einheit.equals(E21)) {
            Ergebniss = SPetabyte;
        } else if (Einheit.equals(E22)) {
            Ergebniss = SPebibyte;
        }

        if (IndexCB == 1) {
            SCB1 = Ergebniss;
        } else if (IndexCB == 2) {
            SCB2 = Ergebniss;
        } else if (IndexCB == 3) {
            SCB3 = Ergebniss;
        } else if (IndexCB == 4) {
            SCB4 = Ergebniss;
        } else if (IndexCB == 5) {
            SCB5 = Ergebniss;
        } else if (IndexCB == 6) {
            SCB6 = Ergebniss;
        } else if (IndexCB == 7) {
            SCB7 = Ergebniss;
        } else if (IndexCB == 8) {
            SCB8 = Ergebniss;
        } else if (IndexCB == 9) {
            SCB9 = Ergebniss;
        } else if (IndexCB == 10) {
            SCB10 = Ergebniss;
        }
    }

    /**
     * Ergebnisse in die Textfelder setzen.
     */
    public void ErgebnisseSetzen() {
        TF1.setText(SCB1);
        TF2.setText(SCB2);
        TF3.setText(SCB3);
        TF4.setText(SCB4);
        TF5.setText(SCB5);
        TF6.setText(SCB6);
        TF7.setText(SCB7);
        TF8.setText(SCB8);
        TF9.setText(SCB9);
        TF10.setText(SCB10);
    }

    /**
     * Fehlermeldung in die Textfelder einsetzen.
     *
     * @param Fehlermeldung Die jeweilige Fehlermeldung.
     */
    public void FehlerSetzen(String Fehlermeldung) {
        TF1.setText(Fehlermeldung);
        TF2.setText(Fehlermeldung);
        TF3.setText(Fehlermeldung);
        TF4.setText(Fehlermeldung);
        TF5.setText(Fehlermeldung);
        TF6.setText(Fehlermeldung);
        TF7.setText(Fehlermeldung);
        TF8.setText(Fehlermeldung);
        TF9.setText(Fehlermeldung);
        TF10.setText(Fehlermeldung);
    }

    /**
     * Setzt das Label auf die in der entsprechendes ComboBox ausgewählten Einheit.
     *
     * @param CB ComboBox bei welcher das Label geändert ändert muss.
     * @param L  Label welches geändert wird.
     */
    public void LabelSetzen(ComboBox CB, Label L) {
        L.setText(CB.getValue().toString() + ":");
    }

    /**
     * Handelt die entsprechende ComboBox
     */
    public void CB1() {
        LabelSetzen(CB1, L1);
    }

    /**
     * Handelt die entsprechende ComboBox
     */
    public void CB2() {
        LabelSetzen(CB2, L2);
    }

    /**
     * Handelt die entsprechende ComboBox
     */
    public void CB3() {
        LabelSetzen(CB3, L3);
    }

    /**
     * Handelt die entsprechende ComboBox
     */
    public void CB4() {
        LabelSetzen(CB4, L4);
    }

    /**
     * Handelt die entsprechende ComboBox
     */
    public void CB5() {
        LabelSetzen(CB5, L5);
    }

    /**
     * Handelt die entsprechende ComboBox
     */
    public void CB6() {
        LabelSetzen(CB6, L6);
    }

    /**
     * Handelt die entsprechende ComboBox
     */
    public void CB7() {
        LabelSetzen(CB7, L7);
    }

    /**
     * Handelt die entsprechende ComboBox
     */
    public void CB8() {
        LabelSetzen(CB8, L8);
    }

    /**
     * Handelt die entsprechende ComboBox
     */
    public void CB9() {
        LabelSetzen(CB9, L9);
    }

    /**
     * Handelt die entsprechende ComboBox
     */
    public void CB10() {
        LabelSetzen(CB10, L10);
    }

    /**
     * Methode des Interface Initializable um Methoden beim Aufruf einer FXML Datei auszuführen.
     * Gibt allen ComboBoxen die entsprechenden ItemListen und wählt die erste automatisch aus.
     * Außerdem führt es jede ComboBox Methode einmal aus damit die Lables richtig gesetzt werden.
     *
     * @param location  Standard im Interface
     * @param resources Standard im Interface
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        CBEingabe.setItems(optionsDataSize);
        CB1.setItems(optionsDataSize);
        CB2.setItems(optionsDataSize);
        CB3.setItems(optionsDataSize);
        CB4.setItems(optionsDataSize);
        CB5.setItems(optionsDataSize);
        CB6.setItems(optionsDataSize);
        CB7.setItems(optionsDataSize);
        CB8.setItems(optionsDataSize);
        CB9.setItems(optionsDataSize);
        CB10.setItems(optionsDataSize);
        CBEingabe.getSelectionModel().selectFirst();
        CB1.getSelectionModel().select(1);
        CB2.getSelectionModel().select(2);
        CB3.getSelectionModel().select(3);
        CB4.getSelectionModel().select(4);
        CB5.getSelectionModel().select(5);
        CB6.getSelectionModel().select(6);
        CB7.getSelectionModel().select(7);
        CB8.getSelectionModel().select(8);
        CB9.getSelectionModel().select(9);
        CB10.getSelectionModel().select(10);
        CB1();
        CB2();
        CB3();
        CB4();
        CB5();
        CB6();
        CB7();
        CB8();
        CB9();
        CB10();
    }
}
