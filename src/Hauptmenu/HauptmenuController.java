package Hauptmenu;

import Einheitenumrechner.Methoden;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

/**
 * Klasse zur Steuerung des Hauptmenues
 *
 * @author Jannik Tappert
 * @version 1.0
 */
public class HauptmenuController {

    @FXML
    Button BtnZahlensystemumrechner, BtnExit, BtnEinheitenumrechner;

    /**
     * Methode zum Beenden des Programms
     */
    @FXML
    public void BtnExit() {
        System.exit(0);
    }

    /**
     * Methode zum Aufrufen der Übersicht der Einheitenumrechner
     */
    @FXML
    public void BtnEinheitenumrechner() {
//Verhindert unter Linux eine fehlerhafte Verformung der Uebersicht
        Methoden.FXMLLoad("/Einheitenumrechner/VolumenRechner.fxml");
        Methoden.FXMLLoad("/Einheitenumrechner/EinheitenumrechnerUebersicht.fxml");
    }

    /**
     * Methode zum Aufrufen des Zahlensystemsumrechner
     */
    @FXML
    public void BtnZahlensystemumrechner() {
        Methoden.FXMLLoad("/Zahlensystemumrechner/Zahlenumrechner.fxml");
    }

    /**
     * Methode zum Aufrufen des Internetgeschwindigkeitsumrechners
     */
    @FXML
    public void BtnInternet() {
        Methoden.FXMLLoad("/Internetgeschwindigkeitsrechner/Internet.fxml");
    }
}


